// Load the TCP Library
var net = require('net'),
util = require('util')
namegen = require('./namegen');

// Default config
var config = { mode:'client', server:{ host:'127.0.0.1', port:1337 } };

// Update config with args
var mode = process.argv[2];

if (mode == "--s") {
	var port = parseInt(process.argv[3]);
	config.mode = 'server';
	if (!isNaN(port))
	{
		config.server.port = port;
	}
}
else if (mode == "--c" && process.argv[3])
{
	var server = process.argv[3].split(":");
	config.server.host = server[0];
	if (server[1]) config.server.port = server[1];
}

if (config.mode == 'server')
{
	// Put a friendly message on the terminal of the server.
	console.log(util.format("Chat server running at port %d\n", config.server.port));

	// Keep track of the chat clients
	var clients = [];

	// Start a TCP Server
	net.createServer(function (socket) {

	  // Identify this client
	  socket.name = namegen.generate();

	  // Put this new client in the list
	  clients.push(socket);

	  // Send a nice welcome message and announce
	  socket.write(JSON.stringify({ name:"SERVER", message:util.format("Welcome to socket-chat %s!", socket.name) }));
	  broadcast(util.format("%s joined the chat", socket.name), socket, true);

	  // Handle incoming messages from clients.
	  socket.on('data', function (data) {
		var message = (data+"").trim();
		if (message.substring(0,5) == "/name")
		{
			var currentName = socket.name;
			socket.name = message.substring(6).trim();
			broadcast(util.format("%s is now known as %s", currentName, socket.name), socket, true);
		} else {
			broadcast(message, socket);	
		}
		if (message.substring(0,4) == "?who")
		{
			var who = clients.map(function(c){ return c.name });
			broadcast("WHO:"+who.join(", "), null, true);
		}
	  });

	  // Remove the client from the list when it leaves
	  socket.on('end', function () {
		clients.splice(clients.indexOf(socket), 1);
		broadcast(util.format("%s left the chat.", socket.name), socket, true);
	  });
	  
	  // Handle errors
	  socket.on('error', function(err) {
		if (err.code == 'ECONNRESET') {
			clients.splice(clients.indexOf(socket), 1);
			broadcast(util.format("%s left the chat.", socket.name), socket, true);
			console.log(util.format("Client %s disconnected", socket.name))
		}
	  });
	  
	  // Send a message to all clients
	  function broadcast(message, sender, serverEvent) {
		clients.forEach(function (client) {
		  // Don't want to send it to sender
		  if (client === sender) return;
		  client.write(JSON.stringify({ name:serverEvent?"SERVER":sender.name, message:message }));
		});
		// Log it to the server output too
		console.log((serverEvent?"SERVER":sender.name) + ": " + message);
	  }

	}).listen(config.server.port, config.server.host);
} else {
	// Put a friendly message on the terminal of the server.
	console.log("Connecting to Chat server at "+config.server.host+":"+config.server.port+"\n");

	var client = new net.Socket();
	client.name = "Me";
	
	// Connect to TCP Server
	client.connect(config.server.port, config.server.host, function () {
		console.log('Connected');
	});

	// Handle incoming messages from clients.
	client.on('data', function (data) {
		data = JSON.parse(data);
		var name = data.name;
		var message = data.message;
		var timestamp = new Date().toISOString().replace('T', ' ').substr(0, 19);
		//process.stdout.write('\033[1'); //clear current line (prompt will rewrite buffer)
		
		console.log(util.format('[%s] %s> %s',name,timestamp,message));
		
		prompt();
		//client.destroy(); // kill client after server's response
	});

	client.on('close', function () {
		console.log('Connection closed');
	});
	
	process.stdin.setEncoding('utf8');
	//process.stdin.setRawMode(true);

	//var buffer = "";
	
	process.stdin.on('data', function (buffer) {
		//buffer += key;
		//process.stdout.write(key);
		//if (key == '\u000d')
		//{
			client.write(buffer.trim());
			if (buffer.substring(0,5) === '/quit') {
				done();
			}
			if (buffer.substring(0,5) === '/name') {
				client.name = buffer.substring(6).trim();
			}
			prompt();
			//buffer = "";
			//process.stdout.write('\n');
		//} else if (key === '\u0003' ) {
		//	done();
		//}
	});
	
	function prompt() {
		process.stdout.write(client.name+'> ');
	}
	
	function done() {
		process.exit();
	}
}