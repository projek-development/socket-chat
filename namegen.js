var NameGen = function () {
	var length = 5;
	var characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	this.generate = function() {
		var name = "Anon_";
		for(var i=0; i<length; i++)
		{
			name += characters[Math.floor(Math.random()*characters.length)];
		}
		return name;
	}
	return this;
};

module.exports = NameGen();